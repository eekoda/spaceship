# Rocket game

--------------------------------------------
### How to use index.js?
Open command promt. Go to location, using "**cd**", where "**..\spaceship**" is located on your computer.

Example:
```
C:\Users\Kasutaja\vscode\Repod\spaceship>
```
After that write **node index.js**. Make sure you have Node installed.

Game will be then activated.

-------------------------------------------

### What does **index.js** do?

Playes rocket game and tries not to destory rockets engine.
Game will last 60 seconds or when you change **timer** variable value in **index.js** file.
App displays time and rockets status.

Example:
```
Time: 1s
{ flowRate: 'OPTIMAL', intermix: 0.5 }
```

Status:
    
    flowRate - this shows how much of both matter and antimatter is being injected into the chamber. It can be OPTIMAL, meaning flow rate is good, HIGH, meaning too much of components are being injected or LOW, meaning not enough is being injected. Keep this in mind when making adjustments.
    
    intermix - number between 0 - 1, if it is less than 0.5, it means more antimatter than matter is being injected, if it's more than 0.5 it means more matter than antimatter is being injected.