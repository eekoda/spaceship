import fetch from "node-fetch";

// Timer for how many seconds have program worked.
let timePassed = 0;
// Timer for how long will program work in seconds.
let timer = 60;

async function start() {
    // Fetches api/start. Starts rocket.
    let startResponse = await postData('https://warp-regulator-bd7q33crqa-lz.a.run.app/api/start', {
        "name": "Eerik Kodasma",
        "email": "kodasmaeerik@gmail.com"
      }, true);

    // Checks if api response was successful.
    if (startResponse['status'] === 'OK') {
        // Gets authorizationCode from response.
        let authorizationCode  = startResponse['authorizationCode'];

        // Game is played as long as timepassed is less than how long game can be played.
        while (timePassed < timer) {
            // Waits a second and adds to timePassed.
            await sleep(1000);
            timePassed++;

            // Logs how many seconds have passed.
            console.log("Time: " + timePassed + "s");

            // Checks what is the rockets status. Fetches api/status.
            let statusResponse = await getData('https://warp-regulator-bd7q33crqa-lz.a.run.app/api/status?authorizationCode=' + authorizationCode, {});
            // Logs status response.
            console.log(statusResponse)

            // Checks if rocket status is okei.
            if (statusResponse['statusCode'] == '400'){
                console.log("You failed!");
                break;
            }else {
                // Calculates how much intermix difference from the value 0.5 . Example: 0.56 => 0.06
                let intermix = (Math.round((statusResponse['intermix'] - 0.5) * 100) / 100);
                // CHecks flowRate from response.
                switch (statusResponse['flowRate']) {
                    case 'LOW':
                        if (intermix < 0) {
                            // Calculates how much 0.2 difference from the value intermix
                            let roundedNum = Math.round((0.2 + intermix) * 100) / 100;
                            // Call Matters function.
                            changeMattersValue(authorizationCode, 0.2)
                            // Call AntiMatters function.
                            changeAntiMattersValue(authorizationCode, roundedNum)
                        }else {
                            let roundedNum = Math.round((0.2 - intermix) * 100) / 100;
                            changeMattersValue(authorizationCode, roundedNum)
                            changeAntiMattersValue(authorizationCode, 0.2)
                        }
                        break;
                    case 'OPTIMAL':
                        if (intermix < 0) {
                            let roundedNum = Math.round((0.2 + intermix) * 100) / 100;
                            changeMattersValue(authorizationCode, 0.2)
                            changeAntiMattersValue(authorizationCode, roundedNum)
                        }else {
                            let roundedNum = Math.round((0.2 - intermix) * 100) / 100;
                            changeMattersValue(authorizationCode, roundedNum)
                            changeAntiMattersValue(authorizationCode, 0.2)
                        }
                        break;
                    case 'HIGH':
                        if (intermix < 0) {
                            let roundedNum = Math.round((0.2 + intermix) * 100) / 100;
                            changeMattersValue(authorizationCode, -roundedNum)
                            changeAntiMattersValue(authorizationCode, -0.2)
                        }else {
                            let roundedNum = Math.round((0.2 - intermix) * 100) / 100;
                            changeMattersValue(authorizationCode, -0.2)
                            changeAntiMattersValue(authorizationCode, -roundedNum)
                        }
                        break;
                }
            }


        }
    }
  }

async function postData(url = '', data = {}, forStart) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    if (forStart) {
        return response.json();
    }
    return response; // parses JSON response into native JavaScript objects
}

async function getData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

function changeMattersValue(authCode, intermixValue) {
    // Changing Matters value. Using api/adjust/matter.
    postData('https://warp-regulator-bd7q33crqa-lz.a.run.app/api/adjust/matter', {
        "authorizationCode": authCode,
        "value": intermixValue
    }, false);
}

function changeAntiMattersValue(authCode, intermixValue) {
    // Changing AntiMatters value. Using api/adjust/matter.
    postData('https://warp-regulator-bd7q33crqa-lz.a.run.app/api/adjust/antimatter', {
        "authorizationCode": authCode,
        "value": intermixValue
    }, false);
}

function sleep(ms) {
    // Sleep for how many seconds.
    return new Promise(
        resolve => setTimeout(resolve, ms)
    );
}

// Starts program.
start();
  